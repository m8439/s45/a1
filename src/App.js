import {useState, useEffect} from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import {UserProvider} from './UserContext'

import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from  './pages/Logout'
import ErrorPage from  './pages/ErrorPage'


import AppNavBar from './components/AppNavBar'
import Footer from  './components/Footer'


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    email: null
  })

  const token = localStorage.getItem("token")

  useEffect( () =>{
    fetch('http://localhost:3007/api/users/profile',{
      method: "GET",
      headers: {
        "Authorization": `Bearer ${token}`
      }
    })
    .then(response => response.json())
    .then(response => {
      // console.log(response)
      setUser({
        id: response._id,
        isAdmin: response.isAdmin,
        email: response.email
      })
    })

  }, [])

  return(

    <UserProvider value={{user, setUser}} >
      <BrowserRouter>
        <AppNavBar/>
        <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/courses" element={<Courses/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="/login" element={<Login/>} />
          <Route path="/logout" element={<Logout/>} />
          <Route path="*" element={<ErrorPage/>} />
        </Routes>
        <Footer/>
      </BrowserRouter>
    </UserProvider>
    
  )
}

export default App;


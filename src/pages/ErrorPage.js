
import Banner from './../components/Banner'

export default function ErrorPage(){

	const data = {
		title: "Error 404",
		description: "Page Not Found",
		destination: "/",
		buttonDesc: "Go back home"
	}

	return(
		<Banner bannerProp={data}/>
	)
}
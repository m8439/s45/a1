import {Fragment} from 'react';
import Banner from './../components/Banner'
import Highlights from './../components/Highlights'

export default function Home(){

	const data = {
		title: "Welcome to Course Booking",
		description: "Opportunities for everyone, everywhere",
		destination: "/courses",
		buttonDesc: "Enroll now"
	}


	return (
		//render navbar, banner & footer
		<Fragment>
			<Banner bannerProp={data}/>
			<Highlights/>
		</Fragment>

	)
}
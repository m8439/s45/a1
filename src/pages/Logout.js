import {useContext, useEffect} from 'react'
import UserContext from	'./../UserContext'
import {Navigate} from 'react-router-dom'

export default function Logout() {
	// console.log(localStorage)

	localStorage.clear()


	const {user, setUser} = useContext(UserContext)
	// console.log(user)

	useEffect( () =>{
		setUser({
			id:null,
			isAdmin:null
		})
	})

	return(
		
		<Navigate to="/login"/>
	)
}
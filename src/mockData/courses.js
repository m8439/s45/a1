

let coursesData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, iure delectus veritatis officiis placeat expedita maxime cumque provident excepturi assumenda, dicta, ipsam? Adipisci hic quia magnam quae illum nulla libero!",
		price: 25000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, iure delectus veritatis officiis placeat expedita maxime cumque provident excepturi assumenda, dicta, ipsam? Adipisci hic quia magnam quae illum nulla libero!",
		price: 35000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, iure delectus veritatis officiis placeat expedita maxime cumque provident excepturi assumenda, dicta, ipsam? Adipisci hic quia magnam quae illum nulla libero!",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "NodeJS-ExpressJS",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, iure delectus veritatis officiis placeat expedita maxime cumque provident excepturi assumenda, dicta, ipsam? Adipisci hic quia magnam quae illum nulla libero!",
		price: 55000,
		onOffer: false
	}


]

export default coursesData

import {Card, Row, Col} from 'react-bootstrap'

export default function Highlihts(){
	return(
		<Row className="m-5">
			<Col xs={12} md={4} >
				<Card>
				  <Card.Body>
				    <Card.Title>Learn From Home</Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, iure delectus veritatis officiis placeat expedita maxime cumque provident excepturi assumenda, dicta, ipsam? Adipisci hic quia magnam quae illum nulla libero!
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4} >
				<Card>
				  <Card.Body>
				    <Card.Title>Study Now, Pay Later</Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, iure delectus veritatis officiis placeat expedita maxime cumque provident excepturi assumenda, dicta, ipsam? Adipisci hic quia magnam quae illum nulla libero!
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4} >
				<Card>
				  <Card.Body>
				    <Card.Title>Be Part of Our Community</Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, iure delectus veritatis officiis placeat expedita maxime cumque provident excepturi assumenda, dicta, ipsam? Adipisci hic quia magnam quae illum nulla libero!
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
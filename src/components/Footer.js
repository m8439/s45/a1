
export default function Footer(){
	return(

		<div className="bg-info text-white d-flex justify-content-center align-items-center mt-1 fixed-bottom" style ={{height:'5vh', marginTop: '50px'}}>
	        <p className="m-0 font-weight-bold"> Mark &#64; Course booking | Zuitt Coding Bootcamp &#169;
	        </p>
	    </div>
	)

}

import {Fragment, useContext} from 'react'
import {Navbar, Container, Nav} from 'react-bootstrap'
import UserContext from './../UserContext'

export default function AppNavBar(){
	const { user } = useContext(UserContext)
	console.log(user)

	// const email = localStorage.getItem('email')
	// console.log(user)

	return(
		<Navbar expand="lg" bg="info" variant="dark">
		  <Container>
		  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
		  <Navbar.Collapse id="responsive-navbar-nav">
		    <Nav className="me-auto">
		      <Nav.Link href="/" className="text-light">Home</Nav.Link>
		      <Nav.Link href="./courses" className="text-light">Courses</Nav.Link>
		      {
		      	user.email !== null 
		      	?
		      	<Nav.Link href="./logout" className="text-light">Logout</Nav.Link>
		      	:
		      	<Fragment>
		      		<Nav.Link href="./login" className="text-light">Login</Nav.Link>
		      		<Nav.Link href="./register" className="text-light">Register</Nav.Link>
		      	</Fragment>
		      }
		      
		      
		    </Nav>
		  </Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}
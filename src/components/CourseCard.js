
import {useState,useEffect} from 'react'
import {Card, Button} from 'react-bootstrap'

export default function CourseCard({courseProp}){
	//console.log(prop)
	// console.log(courseProp)

	let [count, setCount] = useState(0)

	const {name, description, price} = courseProp
	// console.log(name)
	// console.log(description)
	// console.log(price)

	useEffect( () => {
		// console.log('render')
	}, [count])

	const handleClick =() =>{
		// console.log(`I'm clicked`, count + 1)
		// count++ cannot be
		if(count <= 30){
			setCount(count + 1)
			if (count === 30) {
				setCount(count)
				alert(`No more availbale seats`)
			}		
		}
		
	}

	return (

		<Card className="m-5">
			  <Card.Body>
			    <Card.Title> {name} </Card.Title>
			    <Card.Subtitle>Description</Card.Subtitle>
			    <Card.Text>
			       {description}
			    </Card.Text>
			    <Card.Subtitle>Price</Card.Subtitle>
			    <Card.Text>
			       {price}
			    </Card.Text>
			    <Card.Subtitle>Enrolees</Card.Subtitle>
			    <Card.Text> {count} Enrolees
			    </Card.Text>
			    <Button className="btn btn-info" onClick={handleClick} >Enroll</Button>
			  </Card.Body>
			</Card>
	)
}